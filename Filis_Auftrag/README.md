![TOC]

# Filius - Netzwerk für ein KMU einrichten

In diesem Auftrag mussten wir im Filius alle **IP-Adressen**, **Default Gateways**, den **DNS IP** in jedem Client einfügen und die **Subnetzmaske** mit den **/8 /16 /24**... <br>

<br>

Bei jedem Client geben wir die Angaben vom Auftrag an:<br>
![Architektur](./images/angaben.png)<br>

Zuerst habe ich die IP-Adressen, Subnetz, Default Gateways und die IP-Adresse des DNS eingegeben:<br>
![Architektur](./images/angaben_clients.png)<br>

Beim DNS eben alle Clients und anderen Server eingeben (ausser die Clients, die "DHCP Configuration")<br>
![Architektur](./images/dhcpconfig.png)<br>
![Architektur](./images/dns.png)<br>

Im Web-Server habe ich den Webserver hinzugefügt und aktiviert:<br>
![Architektur](./images/webserver.png)<br>

Das bei den Clients hinzufügen:<br>
![Architektur](./images/webbrowser.png)<br>

In einem Client kontrollieren, ob man google suchen kann:<br>
![Architektur](./images/ping_google.png)<br>
![Architektur](./images/filius_webserver.png)<br>

<br>

Ja... also PC11 kann sich mit PC51 pingen:
![Architektur](./images/ping_PC11.png)<br>
Das gleiche geht mit PC51...<br>

Also, es sollte eine Quick-Doku werden, höre jetzt auf mit schreiben...