[TOC]

# DNS Konfigurieren im Filius
Unsere Aufgabe war es in Filius einen lokalen **DNS-Server** zu konfigurieren.<br>
![Architektur](./images/DNS.png)
<br>

<br>

## IP-Konfiguration Clients
Im Filius haben wir eine Vorlage gekriegt.<br>


Wir fügen die IP-Adresse des DNS-Server in jedem Client hinzu:<br>
![Architektur](./images/DNS_IP.png)<br>

Bei **Domain Name Server** gibt man die IP-Adresse des DNS-Servers ein.<br>
![Architektur](./images/IP_Client.png)<br>

Als nächsten Schritt gehen wir zum DNS-Server auf dieses Symbol:<br>
![Architektur](./images/DNS_Server.png)<br>

Und bei **Address (A)** gibt man alle Angaben von den Clients an, also die **IP-Adressen und Namen**<br>
![Architektur](./images/addresses.png)<br>

Als letzter Schritt kontrolliere ich, ob sich die Clients untereinander pingen können. Ich pinge von *Verstappen* zu *Leclerc*:<br>
![Architektur](./images/ping.png)<br>
Hier sieht man, dass ich mit demm command ``ping Leclerc`` den Client gepinged habe und dass es funktioniert hat und kein Paket verloren ging.<br>

<br>

<br>

<br>

# DNS Konfigurieren VMware Workstation - Windows Server 2019 (Standard)
Nun lade ich das _ISO-Image_, das ich von TBZ in Microsoft Azure zur Verfügung kriege. Zuerst zeige ich, wie ich alles vorbereite, bevor ich die statische IP für den DNS-Server konfiguriere.<br>

## Installation Windows Server 2019
Als aller erster Schritt klickt man **oben links** im VMware **"Edit"**, dann auf **"New Virtual Machine..."**<br>
![Architektur](./images/1step.png)<br>

Danach wählt man entweder **Typical** oder **Custom** ist eigentlich egal.<br>
![Architektur](./images/2step.png)<br>

Nun kommen die Einstellungen, die man will. Ich habe das meiste so gelassen, wie die VM vorher eingestellt war - das einzige was ich änderte war **der Speicherort der VM** das **richtige _ISO-Image_**
<br>

Wichtig zu beachten ist, dass man es **im lokalen Datenträger** speichert und **nicht** in der Cloud!<br>
![Architketur](./images/speicherort.png)<br>
![Architektur](./images/installerdisc.png)<br>
![Architektur](./images/numprocessor.png)<br>
![Architektur](./images/DISK.png)<br>
![Architektur](./images/NVMe.png)<br>
![Architektur](./images/LSI.png)<br>
![Architektur](./images/MB.png)<br>
![Architektur](./images/NAT.png)<br>
![Architektur](./images/diskfile.png)<br>

<br>

**Ganz wichtig zu beachten ist, dass man es ~~nicht~~ als "Datacenter"<br> speichert, sondern als Standard!!!**<br>
![Architektur](./images/winserverstandard.png)
<br>

Wenn man das alles gemacht hat, dann kann man die Einstellungen **speichern** und auf "Next/Weiter klicken. Dann wird es automatisch ins Windows aufsarten...<br>
![Architektur](./images/finished.png)

<br>

Und dann kommt man in dieses Menü **(oder sucht "Server Manager im Search ein)** <br>
![Architektur](./images/servermanager.png)

<br>

# Installation und Konfiguration DNS
Hier erkläre ich, wie man den DNS installiert und konfiguriert...<br>
<br>

## Installation DNS
Zuerst schauen wir uns, ob die IP-Adresse, Subnetz und Gateway stimmmen.<br>
Das machen wir, indem wir auf dem Wlan-Symbol klicken und dann auf "weitere Adapter-Optionen klicken und dann auf **Internet Protocol Version 4 (TCP/IPv4)** aussuchen... ausserdem noch das **Ipv6 deaktivieren!**<br>
![Architektur](./images/ipv4.png)<br>

Dann tippt man die gewünschte IP-Adresse ein, Gateway und DNS:<br>
![Architektur](./images/ipv4_2.png)<br>

Nun können wir mit der Konfiguration beginnen.<br>

<br>

**Als erstes:**<br>

Geht man zum Server Manager und dann auf **"Manage"** und **"Add Roles and Features"**<br>
![Architektur](./images/servermanager1.png)<br>
![Architektur](./images/addfeatures.png)<br>
<br>

Nun wählt man alles an, das ich ausgewählt habe:<br>
**"Role-based or feature-based installation"** <br>
![Architektur](./images/installationtype.png)<br>
**"Select a server from the pool"**<br>
![Architektur](./images/serverpool.png)<br>
**DNS Server auswähhlen** (das andere kann man lassen) <br>
![Architektur](./images/roles.png)<br>
**...warten und dann auf Install klicken**<br>
![Architektur](./images/installationprog.png)<br>
Danach geht man wieder beim **Server Manager**, **Tools** und dann **DNS**<br>
![Architektur](./images/ToolsDNS.png)<br>

### Forward Lookup Zone

![Architektur](./images/newzone.png)<br>
![Architektur](./images/primaryzone.png)<br>
![Architektur](./images/zone_name.png)<br>
![Architektur](./images/zonefile.png)<br>
![Architektur](./images/donotallowdynamicupdates.png)<br>
![Architektur](./images/finishednewzonewizard.png)<br>
so sieht's dann aus: <br>
![Architektur](./images/zone_name_finish.png)<br>

Nun kann man mit dem **Reverse Lookup** starten.
Hier geht man auch genau gleich vor wie beim vorherhingen Schritt: <br>

### Reversed Lookup Zone

![Architektur](./images/resverselookup_newzone.png)<br>
![Architektur](./images/newzone_2.png)<br>
Hier zu beachten ist, dass man **IPv4 Reverse Lookup Zone** wählt!<br>
![Architektur](./images/reverselookup_name.png)<br>
Achtung: Hier gebt man die **ersten 3 Oktett der IP-Adresse an**<br>
![Architektur](./images/threeoct.png)<br>
den Namen kann man so lassen<br>
![Architektur](./images/zonefile_name.png)<br>
so sollte es nun aussehen: <br>
![Architektur](./images/reverselookupzone.png)<br>

### New Host (Server)
Nun braucht es noch den Server und den Client.
Mit Doppelklick auf **sotagmbh.ch** und dann auf **New Host (A or AAA)** klicken<br>
![Architektur](./images/newhost1.png) <br>
Hier gibt man noch die IP-Adresse des Servers ein **192.168.162.128** und **Create associated pointer (PTR) record** anklicken!!! <br>
![Architektur](./images/newhost_server.png)<br>
diese Meldung erscheint dann, wenn alles geklappt hat :3 <br>
![Architektur](./images/dns_succ.png)<br>

Jetzt  muss man noch den Host für den Client hinzufügen...<br>

### New Host (Client)
Hier geht man genau gleich vor wie beim Host für den Server.<br>
Zu beachten ist jedoch, die IP-Adresse des Clients benutzen!<br>
![Architektur](./images/newhost_client.png)<br>
![Architektur](./images/endresulthosts.png)<br>

### Forwarders
Jetzt braucht es noch den DNS-Forwarder. Den benötigt man, um DNS-Anfragen von einem lokalen Netzwerkgerät an einen anderen DNS-Server weiterzuleiten. <br>

Mit Doppelklick auf den Server und dann auf **Propterties** klicken.<br> 
![Architektur](./images/idk.png)<br>
Auf **Forwarders** klicken, dann auf **Edit** <br>
![Architekutr](./images/forwarders.png)<br>
Hier einfach die IP von Google eingeben (geht auch 8.8.8.8)<br>
![Architektur](./images/dnsgoogle.png)<br>

### Testing
Nun kann man mit commands checken, ob der DNS-Server auch funktioniert!<br>

Unter **Launch nslookup** kann man kontrollieren, ob's funktioniert<br>
![Architektur](./images/nslookup.png)<br>

**Es sollten Server und IP-Adresse angegeben sein - es funktioniert! :)**<br>
![Architektur](./images/nslookuplaunched.png)<br>

Unter **CMD** kann man auch diese Commands eingeben:<br>

- ``nslookup server.sotagmbh.ch localhost``<br>
![Architektur](./images/testing.png)<br>

- ``nslookup server.sotagmbh.ch``<br>
- ``nslookup client.sotamgbh.ch``<br>
![Architektur](./images/testingfinish.png)<br>

### Ping DNS-Google / 20min.ch
Hier noch wie der Ping mit DNS-Google und mit 20min.ch funktioniert:<br>

- ``ping dns.google``<br>
![Architektur](./images/pingingdnsgoogle.png)<br>

- ``ping 20min.ch``<br>
![Architektur](./images/ping_20min.png)<br>

Das geht auch mit Youtube, Spotify und so weiter... (^_^)<br>

- ``ping youtube.ch``<br>
![Architektur](./images/pingyoutube.png)<br>
<br>

### Ping Reverse Lookup
Mit dem Reverse Lookup schreibe ich statt Hostname, die **IP-Adresse**<br>

- ``nslookup 192.168.162.130``<br>
![Architektur](./images/reversenslookup.png)<br>
<br>

### That's it! 🥳