![TOC]
# Auftrag Print-Server

1. Tools -> **Print Management**<br>
![Architektur](./images/step1.png)<br>


2. Auf Driver **Add Driver** wählen<br>
![Architektur](./images/step2(richtig).png)<br>

3. auf die zweite Option **Add a TCP/IP or Web Services Printer by IP address or hostname**<br>
![Architektur](./images/step17.png)<br>

4. **Hostname/IP und Portname** vom Drucker eingeben<br>
![Architektur](./images/step18.png)

5. den Treiber wählen **HP Universal Printing PCL 6**<br>
![Architektur](./images/step20.png)<br>
![Architektur](./images/step21.png)<br>

6. und zuletzt eine Testseite ausdrucken - es funktioniert.<br>
![Architektur](./images/step13_testpage.png)<br>

<br>

Ich kann mich ebenfalls mit dem Drucker pingen.<br>
![Architektur](./images/step_ping.png)<br>