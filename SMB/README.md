[TOC]

# Installation Ubuntu Samba - 22.04.3 LTS
In dieser Aufgabe zeige ich, wie man Samba installiert 🥴

Zuerst einmal brauchen wir **Ubuntu 22.04.3 LTS (Desktop)**.

## SMB Installation
Als aller erstes gehen wir ins CMD und führen diesen command aus:<br>

- ``sudop apt-get install``<br>
![Architektur](./images/screenshot1.png)<br>

<br>

Dann muss man die Paketliste aktualisieren und Samba mit den **Net-Tools** installieren:<br>

- ``sudo apt-get update && sudo apt-get install samba net-tools -y``<br>
![Architektur](./images/screenshot2.png)<br>
<br>

Nun passt man die Konfigurationen an:

- ``sudo nano /etc/samba/smb.conf``<br>
![Architektur](./images/screenshot3%20(2).png)<br>
![Architektur](./images/screenshot4.png)<br>

<br>

Wenn das gemacht wurde, dann fügt man ein **SambaShare Benutzer** hinzu:<br>

- ``sudo smbpasswd -a sotagmbh``<br>
![Architektur](./images/smbpassword.png)<br>


<br>

Als letzter Schritt führt man einen **Neustart** durch:<br>

- ``sudo service smb restart``<br>
![Architektur](./images/sudoservice.png)<br>

<br>

## Windows Zugriff auf freigegebener Ordner
Nun kann man testen, ob der SMB-Diesnt richtig funktioniert. Zuerst öffnet man den **Explorer** mit der rechten Maustaste und wählt auf **This PC** und **Add Network Location**<br>

![Architektur](./images/winzugriff.png)<br>
![Architektur](./images/winzugriff1.png)<br>

Hier sieht man, dass der Ordner auftaucht, d.h. dass man auf diesen Ordner zugreiffen kann.<br>

### That's it.