[TOC]

<br>

## Einführung
Unsere Aufgabe ist es einen **virtuellen Linux Server** und einen **virtuellen Windows-Clients** zu installieren. Dafür braucht man: VMware Workstation und den ISO-image Ubuntu 22.04

<br>

## VM vorbereiten
Zuerst muss die Konfiguration der VM stimmen. Ich habe einen NAT als **"NAT only"** und den zweiten als **M_117**<br>
![Architektur](./images/image12.png)

Ich habe eine neue VM erstellt und werde nicht zeigen, wie ich das gemacht habe.
Wenn ihr das ISO-image gewählt habt, dann wird sich die VM automatisch starten...

## Ubuntu Konfiguration
Jetzt muss ich einfach paar Konfigs/Anpassungen machen.
<br>

Hier die Sprache auswählen: <br>
![Architektur](./images/image3.png)<br>
Ubuntu Server auswählen:<br>
![Architektur](./images/image13.png)<br>
Dann... ich bin zu faul, man muss einfach alles anpassen wie man's will.

<br>

## Konfiguration DHCP
So, jetzt bin ich beim schlimmsten Part. Wenn man die Ubuntu Konfig geschafft hat, dann kann man sich einloggen mit dem Namen und PW, das man eingegeben hat vorher.

<br>

So sieht's dann aus:<br>
![Architektur](./images/image9.png)

Dann gibt man den command: ``sudo apt update`` ein. <br>
Dannach installiert man den DHCP mit ``sudo apt install isc-dhcp-server -y``<br>
![Architektur](./images/dhcp_install.png)<br>

Nachher ``sudo /etc/default/isc-dhcp-server`` eingeben --> INTERFACEv4=**"ens37"**<br>
![Architektur](./images/INTERFACEv4.png)<br>

Und dann mit ``sudo nano /etc/dhcp/dhcpd.conf`` die Ranges undso anpassen...<br>
![Architektur](./images/dhcp_config.png)<br>

So, dann mit dem command ``cd /etc/netplan/`` und dann ``sudo nano /etc/netplan/00-installer-config.yaml`` um Netzwerk anzupassen. <br>
Man gibt ein für _ens33_ bei _dhcp4_ "true" ein und bei _ens37_ gibt man bei _dhcp4_ "no" ein...<br>
![Architektur](./images/netplan.png)<br>

Und dann mit dem command ``sudo netplan apply`` speichert man die angaben.<br>

``sudo systemctl start isc-dhcp-server`` und ``sudo systemctl enable isc-dhcp-server`` <br
![Architektur](./images/systemctl.png)<br>


## Fehler
Sollte das nun geklappt haben - nur funktioniert es bei mir irgendwie nicht.<br>
Ich kriege nämlich immer diese Meldungen:<br>
![Architektur](./images/netplan_apply.png)<br>
![Architektur](./images/fehlermeldung.png)<br>

Ich habe alles versucht, aber ich verstehe es einfach wirklich nicht.<br>
![Architektur](./images/ip_a.png)<br>

Ich habe mehrere Videos angeschaut, mehrere Anleitungen... ich habe sogar ChatGPT nachgefragt und mehrere commands wie
- ``ifconfig``
- ``sudo cat /var/log/syslog | grep dhcp``
<br>

und irgendwie ging alles trotzdem nicht... Ich habe sehr viel Zeit investiert, mehrere Stunden - 3 Tage lang. Habe 2 zusätzliche VMs erstellt, genau gleich. Habe eine neue VM zu Hause versucht zu erstellen, aber habe irgendwie keine Verbindung?<br>
![Architektur](./images/eth.jpg)<br>

Ich habe auch andere Mitschüler gefragt, bei denen es geklappt hat, aber sie konnten den Fehler auch nicht finden, bzw. mir nicht weiterhelfen...
<br>

Ich mag nicht mehr :-(
<br>
