[TOC]

### **Lernziele**
Kennt den prinzipiellen Unterschied zwischen einer dynamischen und einer statischen Zuweisung von IP Adressen und kann aufzeigen, in welchem Anwendungsfall eine dynamische resp. eine statische Adress-Zuweisung sinnvoll ist.

## **1. Router-Konfiguration auslesen**
Im ersten Schritt gilt es herauszufinden in welchem Subnet und welchem Range der DHCP Server IPv4-Adressen im lokalen Netzwerk verteilt. Dafür gibt verschiedene Befehle, die in Ihrem Output die entsprechenden Optionen beinhalten. Die Befehle können in der CLI des Routers ausgeführt werden.
Damit die Befehle ausgeführt werden können, muss sich der Router im richtigen Modus befinden. Öffnen Sie die CLI und drücken Sie solange die Enter Taste bis R1> oder ähnlich erscheint. Der Router ist nun bereit für ihre Befehle.
Das > Zeichen in R1> zeigt dem Administrator an, dass er sich im User Mode befindet. Ist dies der Fall, wechseln Sie in den Enabled Mode indem sie den Befehl enable eingeben und anschliessend Enter drücken. Ob erfolgreich in den enabled-Mode gewechselt wurde, sehen Sie daran, dass nun das #-Symobol in der Befehlzeile steht.

- _show ip dhcp pool_
- _show ip dhcp binding_
- _show running-config_

<br>

### show ip dhcp pool
hier sieht man die Range von Adressen, leased adressen...
<br>

![Architektur](./images/Image1.png)

<br>

### show ip dhcp binding
im binding kann man die vergebenen ip adressen sehen, das ablauf datum der ip-adressen...
<br>

![Architektur](./images/image2.png)

<br>

### show running-config
hier sieht man, welche adressen __nicht__ mitinbegriffen wurden, also automatisch vergeben wurden. die sind hauptsächlich für geräte, die fixe ip-adressen benötigen; Drucker, Router, DHCP/DNS und weitere Internetdienste wie; Spotify, Netflix, Youtube...
<br>

![Architektur](./images/image3.png)

### **Fragen/Antworten**
- __Für welches Subnetz ist der DHCP Server aktiv?__

    Der DHCP-Server ist für CIDER /24 aktiv
- **Welche IP-Adressen ist vergeben und an welche MAC-Adressen? (Antwort als Tabelle)**
    | PC Name | IP-Adresse   | MAC-Adresse  |
    |:------- |:------------:| ------------:|
    |PC0      |192.168.35.23 |0009.7CA6.4CE3|
    |PC1      |192.168.35.24 |0001.632C.3508|
    |PC2      |192.168.35.25 |0050.0F4E.1D82|
    |PC3      |192.168.35.26 |00E0.8F4E.65AA|
    |Server0  |192.168.35.27 |0007.ECB6.4534|
    |Laptop0  |192.168.35.28 |00D0.BC52.B29B|

- **In welchem Range vergibt der DHCP-Server IPv4 Adressen?**

    Im Range: 192.168.35.1 - 192.168.35.254

- **Was hat die Konfiguration ip dhcp excluded-address zur Folge?**

    d.h. nur, dass diese IP-Adressen **nicht** zur Verfügung stehen
    z.B. für statische IP-Adressen/fixe IP-Adresse... nur; 192.168.35.1 - 192.168.35.22 / 192.168.35.146 - 192.168.35.230

- **Wie viele IPv4-Adressen kann der DHCP-Server dynamisch vergeben?**



## **2. DORA - DHCP Lease beobachten**
Mit dem Cisco Packet Tracer können Ethernet Frames oder allgemeiner Protocol Data Unit (PDU) sehr gut nachverfolgt werden. In diesem Schritt soll eine DHCP IPv4-Adress-Zuteilvorgang mitverfolgt werden.

- PC2 dazu bringen eine neuen DHCP-Request zu starten. Dafür im Menü des PCs unter Config => FastEthernet0 die IP Konfiguration kurz von DHCP auf Static setzen.
<br>

![Architektur](./images/image9.png)
- Unten rechts im Packet Tracer auf Simulation wechseln.
<br>

![Architektur](./images/image10.png)
- Im Menü des PC2 unter Config => FastEthernet0 die IP Konfiguration kurz von Static wieder auf DHCP setzen.
- PC2 wieder starten.
<br>

![Architektur](./images/image11.png)
- Es erscheint sofort ein gelbes PDU beim PC0.
<br>

![Architektur](./images/image4.png)
- Mit einem klick kann das PDU geöffnet und analysiert werden.

### **Fragen/Antworten**
- Welcher OP-Code hat der DHCP-Offer?
- Welcher OP-Code hat der DHCP-Request?
- Welcher OP-Code hat der DHCP-Acknowledge?
- An welche IP-Adresse wird der DHCP-Discover geschickt? Was ist an dieser IP-Adresse speziell?
- An welche MAC-Adresse wird der DHCP-Discover geschickt? Was ist an dieser MAC-Adresse speziell?
- Weshalb wird der DHCP-Discover vom Switch auch an PC3 geschickt?
- Gibt es ein DHCP-Relay in diesem Netzwerk? Wenn ja, welches Gerät ist es? - Wenn nein, wie kann das mithilfe der DHCP-PDUs festgestellt werden?
- Welche IPv4-Adresse wird dem Client zugewiesen?

Auf den Screenshots sollten alle Felder von DHCP sichtbar sein:

Screenshot des DHCP-Discovers PDUs
Screenshot des DHCP-Offers PDUs
Screenshot des DHCP-Request PDUs
Screenshot des DHCP-Acknowledge PDUs

## **3. Netzwerk umkonfigurieren**
Aktuell erhält der Server seine IPv4-Adresse vom DHCP-Server. Diese IPv4-Adresse ist nicht statisch zugewiesen und kann sich zum Beispiel beim Neustart des Routers oder des Servers ändern. Grundsätzlich kann der Router auch nach Ablauf der DHCP lease time eine andere IPv4-Adresse zuteilen.
Um das zu verhindern weisen wir dem Server eine statische IPv4-Adresse zu.

#### Hinweise

Die statisch zugewiesene IPv4-Adresse muss ausserhalb des DHCP-Bereichs liegen. Konsultieren Sie hierzu Ihre Antworten aus Aufgabe 1.
Die IPv4-Adresse im Menü des Servers unter Config -> FastEthernet0/0 konfiguriert werden.
Das Gateway kann in Menü unter Config -> Settings festgelegt werden.

Im Laborbericht sind folgende Screenshots zu hinterlegen:

Screenshot Konfigurationsmenü Server mit Sichtbarer IPv4-Adresse vom Interface FastEthernet
Screenshot Konfigurationsmenü Server mit Sichtbarer IPv4-Adresse des Gateways
Screenshot der Webseite des Servers auf einem der PCs inkl. sichtbarer IPv4-Adresse des Servers

Prüfen Sie anschliessend, ob der Server von PC1 erreichbar ist.


ping 192.168.X.X (IP-Adresse des Servers einsetzen) unter Desktop -> Command Prompt

Auf dem Server ist eine Webseite aktiv. Mit dem Webbrowser zugreifen Desktop -> Web Browser